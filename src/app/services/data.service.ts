import { Injectable } from '@angular/core';
import { Data } from '../models/data';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  formDetails: Data[]=[];
  updatedDetails: Data[]=[];
  address:Data[]=[];
  person:Data[]=[];
  data:Data[]=[];
  getIndex:number;

  constructor() {
  }

  addData(formData){
    this.formDetails.push(formData);
  }

  addUpdatedData(newData){
    this.formDetails[this.getIndex]=newData;
  }

  updatedSection(){
    // console.log('dataService',this.formDetails[this.getIndex]);
    return this.formDetails[this.getIndex];
  }

}
