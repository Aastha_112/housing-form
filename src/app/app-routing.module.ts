import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DisplayComponent } from './modules/display/display.component';
import { HomeComponent } from './modules/home/home.component';

const routes: Routes = [
  {
    path:'',
    redirectTo:'home',
    pathMatch:'full'
    // component:HomeComponent
  },
  {
    path:'home',
    component:HomeComponent
  },
  {
    path:'home/:index',
    component:HomeComponent
  },
  {
    path:'display',
    component:DisplayComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
