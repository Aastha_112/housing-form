import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css']
})
export class DisplayComponent implements OnInit {

  constructor(public dataService: DataService,
    private router: Router) { }

  ngOnInit(): void {
  }

  get formData(){
    return this.dataService.formDetails;
  }

  update(i){
    this.router.navigate(['/home',i]);
  }

  getAddress(section){
    return section.address;
  }

  getPerson(address){
    return address.person;
  }

}
