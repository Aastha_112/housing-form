import { ChangeDetectorRef, Component, OnChanges, OnInit } from '@angular/core';
import {ControlContainer,FormArray,FormBuilder,FormControl,FormGroup,Validators,} from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})

export class HomeComponent implements OnInit {
  houseForm: FormGroup;
  // updateForm:FormArray;
  updateForm:any;
  updateAddress:FormGroup;
  updatePerson:any;
  index:number;
  i:number;
  j:number;
  currentIndex:number=0;

  constructor(public fb: FormBuilder, public dataService: DataService, 
    private activatedRoute: ActivatedRoute, private ref: ChangeDetectorRef,) {

  }

  ngOnInit(): void {
    this.houseForm = this.fb.group({
      sections: this.fb.array([]),
    });
    let id = parseInt(this.activatedRoute.snapshot.paramMap.get('index')!);
    this.dataService.getIndex=id;
    if(id>=0){
      this.loadSection(this.dataService.updatedSection());
    }
  }

  get formData() {
    return this.dataService.formDetails;
  }

  newData(){
    return this.dataService.updatedSection();
  }

  // InIt variables

  initSection() {
    return this.fb.group({
      houseName: this.fb.control('', Validators.required),
      houseType: this.fb.control('', Validators.required),
      address: this.fb.array([], Validators.required),
    });
  }

  initAddress() {
    return this.fb.group({
      plot: this.fb.control('', Validators.required),
      area: this.fb.control('', Validators.required),
      pincode: this.fb.control('', [
        Validators.required,
        Validators.min(100000),
        Validators.max(999999),
      ]),
      person: this.fb.array([this.initPerson()], Validators.required),
    });
  }

  initAddress2() {
    return this.fb.group({
      plot: this.fb.control('', Validators.required),
      area: this.fb.control('', Validators.required),
      pincode: this.fb.control('', [
        Validators.required,
        Validators.min(100000),
        Validators.max(999999),
      ]),
      person: this.fb.array([], Validators.required),
    });
  }

  initPerson() {
    return this.fb.group({
      name: this.fb.control('', Validators.required),
      age: this.fb.control('', [Validators.required, Validators.max(100)]),
      gender: this.fb.control('', Validators.required),
    });
  }

  loadSection(data){
    this.currentIndex++;
    const section = this.initSection();
    this.updateForm=section;
    section.patchValue({
        houseName:data.houseName,
        houseType:data.houseType,
        address:data.address.forEach(a => {
          const add:FormGroup=this.initAddress2();
          this.updateAddress=add;
          add.patchValue({
            plot:a.plot,
            area:a.area,
            pincode:a.pincode,
            person:a.person.forEach(p => {
              const per:FormGroup=this.initPerson();
              this.updatePerson=per;
              per.patchValue({
                name:p.name,
                age:p.age,
                gender:p.gender,
              });
               this.j = (add.get('person') as FormArray).controls.push(per);
               this.j--;
               console.log(per.value)
              })
            });
            
            // this.updatePerson=add.value.person,
           this.i=(section.get('address') as FormArray).controls.push(add);
        })
      });
      (this.houseForm.get('sections') as FormArray).controls.push(section);
      // this.updateForm=this.houseForm.get('sections') as FormArray
      console.log(this.updatePerson.value);
  }

  getUpdateForm(){
    
    return this.updateForm.value
  }

  // Get Data

  getSections(form) {
    return form.controls.sections.controls;
  }

  getAddress(form) {
    return form.controls.address.controls;
  }

  getPerson(form) {
    return form.controls.person.controls;
  }

  // Add Data

  addSection() {
    const newHouseForm = this.houseForm.get('sections') as FormArray;
    newHouseForm.push(this.initSection());
  }

  addAddress(i) {
    const address = (this.houseForm.get('sections') as FormArray).controls[i].get('address') as FormArray;
    address.push(this.initAddress());
  }

  addPerson(i, j) {
    const person = ((this.houseForm.get('sections') as FormArray).controls[i].get('address') as FormArray).controls[j].get('person') as FormArray;
    person.push(this.initPerson());
  }

  // Submit and Delete

  onSubmit(i) {
    if(this.currentIndex!=0){
      console.log('update',this.currentIndex);  
      console.log(this.updatePerson.value);
      const newPerson = this.updatePerson.value;
      this.updateAddress.value.person[this.j] = newPerson;
      const newAddress = this.updateAddress.value;
      this.updateForm.value.address[0]=newAddress;
      console.log(this.updateForm.value);
      this.dataService.addUpdatedData(this.updateForm.value);
      this.currentIndex=0;
    } 
    else{
      this.dataService.addData(this.houseForm.value.sections[i]);
    }
    this.onDelete(i);
  }

  onDelete(i) {
    const deleteBtn = <FormArray>this.houseForm.get('sections');
    deleteBtn.removeAt(i);
  }

  onDeleteAddress(i,j){
    const deleteBtn = (this.houseForm.get('sections') as FormArray).controls[i].get('address') as FormArray;
    deleteBtn.removeAt(j);
  }

  onDeletePerson(i,j,k){
    const deleteBtn = ((this.houseForm.get('sections') as FormArray).controls[i].get('address') as FormArray).controls[j].get('person') as FormArray;
    deleteBtn.removeAt(k);
  }

}
