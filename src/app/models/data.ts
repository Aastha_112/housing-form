export interface Data{
    houseName:string;
    houseType:string;
    address:Address[]
}

interface Address {
    plot:string;
    area:string;
    pincode:number;
    person:Person[]
} 

interface Person{
    name:string;
    age:number;
    gender:string;
}
